package CookieBot;
/**
 * This is the model for the 
 * CookieClicker bot
 * 
 * @author James Seibel
 * @version 05-09-2019
 * 
 */

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;

public class CookieClicker implements Runnable
{
	/**
	 * Required Variables
	 */
	
	/** variable holding the cookie on the webpage */
	private WebElement bigCookie;
	
	/** This is the reference to the website */
	private WebDriver driver;
	
	/** This is how we interact with the website */
	private Actions builder;
	
	/**	This action is used to click on the main cookie */
	private Action clickCookie;
	
	/** if true the model is running, if false the model is stopped */
	private boolean running;
	
	/** This is used so that we only check certain things every so often, instead of every
	 * time the clickCookie clicks */
	private int loop;
	
	/** This is the maximum number of any one item that the bot will buy */
	private int maxItems = 50;
	
	private boolean buyItems = false;
	
	/**
	 *  default constructor
	 *  @param newDriver reference to the currently open webpage
	 */
	CookieClicker(WebDriver newDriver)
	{
		// set the driver
		driver = newDriver;
		
		// the program is now running
		running = true;
		
		// this allows the program to interact with the webpage better than an element
		// (although it is more complicated to do so)
		builder = new Actions(driver);
		
		// find the cookie
		bigCookie = driver.findElement(By.id("bigCookie"));		
		// allow us to click the cookie
		clickCookie = builder.moveToElement(bigCookie).doubleClick().build();
		
	}

	@Override
	public void run() 
	{
		// start the main logic loop
		playGame();
	}
	
	/**
	 * This method holds all the logic behind the bot's actions,
	 * it controls: clicking the cookie and buying upgrades/products
	 */
	public void playGame()
	{
		// set the loop to 0
		// (when this gets to 40 the program will check if
		// there are any upgrades or products it can buy)
		loop = 0;
		
		// this array holds every product the bot can buy
		WebElement[] products = new WebElement[16];
		
		// this array holds how many of each product is currently owned by the bot
		int[] productsOwned = new int[16];
		
		// create the upgrade variable, so we can buy
		// upgrades later on
		WebElement upgrade = null;
		
		// print that we are reading in the current number of products
		//System.out.println("reading in current number of products...\n");
		
		// products
		for(int i = 0; i < 16; i++)
		{
			// find the location of every product
			products[i] = driver.findElement(By.id("product" + i));
			
			// find the number of products owned and update the array accordingly
			try
			{
				productsOwned[i] = Integer.parseInt(driver.findElement(By.id("productOwned" + i)).getText());
			}
			catch(Exception e)
			{
				productsOwned[i] = 0;
			}
			// print out the number of each product owned
			//System.out.println("number of product" + i + ": " + productsOwned[i]);
		}
		
		// add extra lines for readability
		//System.out.println("\n\n");
		
		// the model is now running
		running = true;
		
		// run until the GUI updates the running variable
		while(running)
		{
			// this allows the program to buy upgrades and products every 40 clicks (or so)
			if(loop == 40)
			{
				// reset the loop
				loop = 0;
				
				// if the bot is allowed to buy items, continue, otherwise
				if(buyItems)
				{
					// close any achievement windows
					// "framed close sidenote" (class to close multiple windows at once)
					//try
					//{
						// look for the achievement close button
					//	driver.findElement(By.className("close")).click();
					//}
					//catch(Exception e)
					//{
						
					//}
					
					
					try // if this fails, it doesn't mean that the product will fail
					{
						// find the first upgrade
						upgrade = driver.findElement(By.id("upgrade0"));
						
						// buy as many upgrades as we can
						if(!(upgrade == null) && upgrade.getAttribute("class").equals( "crate upgrade enabled"))
						{
							// click on the upgrade
							upgrade.click();
							
							// find the first upgrade again (it will have changed if we bought one)
							//upgrade = driver.findElement(By.id("upgrade0"));
						}
					}
					catch(Exception e)
					{
						// the upgrade may be off the page?
					}


					// see if there are any products we can buy
					for(int i = 0; i < 16; i++)
					{
						try // try to buy a product
						{
							// only buy this product if the number of them is less then the max we have set
							if(productsOwned[i] < maxItems)
							{
								// keep buying these items until we have bought all we can
								while(products[i].getAttribute("class").equals( "product unlocked enabled"))
								{
									// try to click on the product to buy it
									products[i].click();

									// increment the number of these products we own
									productsOwned[i] = productsOwned[i] + 1;
								}
							}

						}
						catch(Exception e)
						{
							System.out.println("Error: can't buy product" + i + " , the window is probably too small");
						}
					}

					/** this is the number of products that we have at least maxItems or more of */
					int numberOfMaxedOutProducts = 0;

					// go through every product and see how many we have of each
					for(int i = 0; i < 16; i++)
					{
						// if we own equal to or more than maxItems of this product
						// increment the numberOfMaxedOutProducts by 1
						if(productsOwned[i] >=  maxItems)
						{
							numberOfMaxedOutProducts++;
						}
					}

					// if all 16 of our products have maxItems number of each owned
					// then increase the total number we can own of each
					if(numberOfMaxedOutProducts == 16)
					{
						maxItems *= 1.5;
					}


				}// end of if buyItems
			}// end of if loop == 40

			// increment the loop so we can try to buy thing every 20 clicks
			loop++;

			// click the cookie
			clickCookie.perform();

		}
	}
	
	/**
	 * This stops the bot's main loop
	 */
	public void stop()
	{
		running = false;
	}
	
	/**
	 * @param newBuyItems set whether the bot is allowed to buy items or not
	 */
	public void setBuyItems(boolean newBuyItems)
	{
		buyItems = newBuyItems;
	}
	
	/**
	 * @return whether the bot is allowed to buy items or not
	 */
	public boolean getBuyItems()
	{
		return buyItems;
	}
}









