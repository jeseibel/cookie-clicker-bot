package CookieBot;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/**
 * This program is a bot to play the website
 * game "cookie clicker"
 * 
 * @author James_Seibel
 * @version 05-09-2019
 * @PO 2241
 */
public class CookieClickerGUI extends Application implements EventHandler<ActionEvent>
{
	
	/** this button starts or stops the model */
	private Button controlButton;
	
	/** this button enables or disables buying upgrades */
	private Button buyButton;
	
	/** This displays the current status of the program (the model is currently running or not) */
	private TextField statusDisplay;
	
	/** This is how we interact with the website */
	private WebDriver driver;
	
	/** This is the reference to the CookieClicker model */
	private CookieClicker gameInterface;
	
	/** If true then the model is running */
	private boolean running = false;
	
	/** this is the reference to the model thread */
	public Thread threa;
	
	/** this is the reference to the GUI window */
	private Stage gui;
	
	@Override
	public void start(Stage primaryStage) 
	{
		// create the start/stop button
		controlButton = new Button();
		controlButton.setText("Start"); // set the text
		controlButton.setOnAction(this); // allow the button to do something when pressed
		//controlButton.setMinHeight(64); // set the size of the button
		//controlButton.setMinWidth(64);
		
		// create the upgrades button
		buyButton = new Button();
		buyButton.setText("enable buying"); // set the text
		buyButton.setOnAction(this); // allow the button to do something when pressed
		//controlButton.setMinHeight(64); // set the size of the button
		//controlButton.setMinWidth(64);
		
		// create the status display TextField
		statusDisplay = new TextField();
		statusDisplay.setText("Ready");
		statusDisplay.setEditable(false);

		
		
		// create the internal GUI
		BorderPane root = new BorderPane();

		// add the control button
		root.setLeft(controlButton);
		// place the control button in the center of the space
		BorderPane.setAlignment(controlButton, Pos.TOP_LEFT);
		
		// add the buy button
		root.setCenter(buyButton);
		// place the control button in the center of the space
		BorderPane.setAlignment(buyButton, Pos.CENTER);
		// disable the buy button (once the model is created then we will
		// enable it)
		buyButton.setDisable(true);
		
		// add the status display
		root.setBottom(statusDisplay);
		
		// set what happens when the program is closed
		primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() 
		{
	          @Override
			public void handle(WindowEvent we) 
	          {
	              System.out.println("Shutting down program...");
	              quitProgram();
	              System.out.println("Shutdown complete");
	          }
	      }); 
		
		// create the scene
		Scene scene = new Scene(root,64*3,64);
		scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());

		// set the icon
		primaryStage.getIcons().add(new Image(getClass().getResourceAsStream("cookieIcon.png")));
		
		// set what is to be displayed
		primaryStage.setScene(scene);
		primaryStage.show();

		// title the window
		primaryStage.setTitle("Cookie Bot");
		
		// set the variable so we can edit this outside the start method
		gui = primaryStage;
		
		// make it so that the gui will go on top of the browser
		primaryStage.setAlwaysOnTop(true);
		
		// set the size of the window
		//primaryStage.setMinHeight((COLUMNS * 64) + 39 + 64);
		//primaryStage.setMinWidth((ROWS * 64) + 16 + 64);
		
		
		
		// Launch Firefox


		try
		{
			// this is required to open firefox
			System.setProperty("webdriver.gecko.driver","exefiles\\geckodriver.exe");

			// print the log to a text file instead of to the console
			System.setProperty(FirefoxDriver.SystemProperty.DRIVER_USE_MARIONETTE,"true");
			System.setProperty(FirefoxDriver.SystemProperty.BROWSER_LOGFILE,"log.txt");

			// launch Firefox
			driver = new FirefoxDriver();

			// set the window size
			//driver.manage().window().setSize(new Dimension(600,600));

			// go to the cookie clicker website
			driver.get("http://orteil.dashnet.org/cookieclicker/");

			// print out when the page loads
			System.out.println("Webpage loaded: " + driver.getTitle());
		}
		catch(Exception e)
		{
			// quit the driver, the connection failed
			driver.quit();
			
			// disable the buttons
			controlButton.setDisable(true);
			buyButton.setDisable(true);
			
			// print the error to the display
			statusDisplay.setText("Connection Error");
		}
	}
	
	/**
	 * main method
	 * @param args
	 */
	public static void main(String [] args) 
	{
		// create the window and start the program
		launch(args);
	}
	
	/**
	 * This is run when the window is closed
	 */
	public void quitProgram()
	{
		// is the model running?
		if(!running)
		{
			// the model isn't running, just close
			// the website and program

			// stop the driver (including all associated threads) and close the website
			driver.quit();

			// close the GUI
			gui.close();
		}
		else
		{
			// the model is running, shut it down
			// then close the GUI

			// stop the model's loop
			gameInterface.stop();

			// change the GUI
			statusDisplay.setText("Stopped");
			// update the button text
			controlButton.setText("Start");

			// the model is no longer running
			running = false;

			// close the model
			threa = null;

			// stop the driver (including all associated threads) and close the website
			driver.quit();

			
			
			// close the GUI
			gui.close();
		}
	}
	
	@Override
	public void handle(ActionEvent event) 
	{
		// did the user press the control button?
		if(event.getSource() == controlButton)
		{
			// is the model running?
			if(!running)
			{
				// create a new interface
				gameInterface = new CookieClicker(driver);
				
				// start a new thread
				threa = new Thread(gameInterface);
				
				// start the thread
				threa.start();
				
				// enable the buy button
				buyButton.setDisable(false);
				
				// update the display text
				statusDisplay.setText("running");
				// update the button text
				controlButton.setText("Stop");
				// update buy button text
				buyButton.setText("enable buying");
				
				// the model is now running
				running = true;
			}
			else
			{
				// the model is running, shut it down

				// stop the model's loop
				gameInterface.stop();
				
				// change the GUI
				statusDisplay.setText("Stopped");
				// update the button text
				controlButton.setText("Start");
				// update buy button text
				buyButton.setText("enable buying");
				
				// disable the buy button
				buyButton.setDisable(true);
				
				// the model is no longer running
				running = false;
				
				// close the model
				threa = null;
			}
			
		}
		
		// did the user press the buy button?
		if(event.getSource() == buyButton && gameInterface != null)
		{
			// toggle if the model is allowed to buy things or not
			if(gameInterface.getBuyItems())
			{
				buyButton.setText("Enable Buying");
				gameInterface.setBuyItems(false);
			}
			else
			{
				buyButton.setText("Disable Buying");
				gameInterface.setBuyItems(true);
			}
		}
		
	}// end of handle method

}// end of class
